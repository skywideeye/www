from flask import Flask
app = Flask(__name__)
import backend
import opencv

@app.route('/api/process')
def analyse_image(data):
    return opencv.haar('cars.xml').process(data.files[0].bytes).as_json()

@app.route('*')
def static_files(*args, **kwargs):
    return serve_static(*args, **kwargs)
